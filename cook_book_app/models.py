from django.db import models
from django.contrib.auth.models import User

class Dish(models.Model):
    """Potrawa"""
    text = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.text
        
class Recipe(models.Model):
    """Przepis na potrawę"""
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        verbose_name_plural = 'recipes'
        
    def __str__(self):
        return self.text[:50] + "..."
