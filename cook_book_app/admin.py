from django.contrib import admin
from cook_book_app.models import Dish
from cook_book_app.models import Recipe

# Register your models here.
admin.site.register(Dish)
admin.site.register(Recipe)
