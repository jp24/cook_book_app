# Generated by Django 3.0.8 on 2020-07-14 19:48

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cook_book_app', '0021_remove_dish_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='dish',
            name='owner',
            field=models.ForeignKey(default=15, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
