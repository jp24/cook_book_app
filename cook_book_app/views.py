from django.shortcuts import render
from .models import Dish
from .models import Recipe
from django.http import HttpResponseRedirect 
from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from .forms import DishForm
from .forms import RecipeForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):    
    return render(request, 'cook_book_app/index.html')

@login_required
def all_dish(request):     
    all_dish = Dish.objects.filter(owner=request.user).order_by(
    'date_added')    
    context = {'all_dish': all_dish}     
    return render(request, 'cook_book_app/all_dish.html', context)

@login_required
def dish(request, dish_id):     
    dish = Dish.objects.get(id=dish_id)
    if dish.owner != request.user:         
        raise Http404
        
    recipes = dish.recipe_set.order_by('-date_added')     
    context = {'dish': dish, 'recipes': recipes}     
    return render(request, 'cook_book_app/dish.html', context)

@login_required
def new_dish(request):    
    if request.method != 'POST':         
        form = DishForm()     
    else:        
        form = DishForm(request.POST)         
        if form.is_valid(): 
            new_dish = form.save(commit=False)
            new_dish.owner = request.user           
            form.save()             
            return HttpResponseRedirect(reverse('cook_book_app:all_dish')) 
            
    context = {'form': form}     
    return render(request, 'cook_book_app/new_dish.html', context)

@login_required
def new_recipe(request, dish_id):    
    dish = Dish.objects.get(id=dish_id)
    if request.method != 'POST':         
        form = RecipeForm()     
    else:        
        form = RecipeForm(data=request.POST)         
        if form.is_valid():            
            new_recipe = form.save(commit=False)             
            new_recipe.dish = dish             
            new_recipe.save()            
            return HttpResponseRedirect(reverse('cook_book_app:dish', args=[dish_id]))
            
    context = {'dish': dish, 'form': form}    
    return render(request, 'cook_book_app/new_recipe.html', context)

@login_required
def edit_recipe(request, recipe_id):    
    recipe = Recipe.objects.get(id=recipe_id)     
    dish = recipe.dish
    if dish.owner != request.user:
        raise Http404
    if request.method != 'POST':        
        form = RecipeForm(instance=recipe)     
    else:        
        form = RecipeForm(instance=recipe, data=request.POST)         
        if form.is_valid():            
            form.save()             
            return HttpResponseRedirect(reverse('cook_book_app:dish', args=[dish.id]))
            
    context = {'recipe': recipe, 'dish': dish, 'form': form}    
    return render(request, 'cook_book_app/edit_recipe.html', context)
