from django.conf.urls import url 
from . import views 

urlpatterns = [     
    # Strona główna.    
    url(r'^$', views.index, name='index'), 
    url(r'^all_dish/$', views.all_dish, name='all_dish'),
    url(r'^all_dish/(?P<dish_id>\d+)/$', views.dish, name='dish'),
    url(r'^new_dish/$', views.new_dish, name='new_dish'),
    url(r'^new_recipe/(?P<dish_id>\d+)/$', views.new_recipe,
    name='new_recipe'),
    url(r'^edit_recipe/(?P<recipe_id>\d+)/$', views.edit_recipe, 
    name='edit_recipe'),
]
