from django import forms
from .models import Dish
from .models import Recipe


class DishForm(forms.ModelForm):     
    class Meta:        
        model = Dish
        fields = ['text']         
        labels = {'text': ''}

class RecipeForm(forms.ModelForm):    
    class Meta:        
        model = Recipe        
        fields = ['text']        
        labels = {'text': ''}         
        widgets = {'text': forms.Textarea(attrs={'cols': 80})}
