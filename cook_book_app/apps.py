from django.apps import AppConfig


class CookBookAppConfig(AppConfig):
    name = 'cook_book_app'
